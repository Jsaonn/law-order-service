from django.views.decorators.http import require_http_methods
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
import requests

from .serializers import OrderDataSerializer, OrderDataGetMenuSerializer, OrderMenuDataSerializer
from .models import OrderData, OrderMenuData

import os
import json


@require_http_methods(['GET'])
@api_view(['GET'])
def get_order_by_id(request, pk):
    order = get_object_or_404(OrderData, pk=pk)
    serializer = OrderDataGetMenuSerializer(order)

    send_logs('get_order_by_id', {}, '', pk, serializer)

    return Response(
        {"data": serializer.data}
    )

@require_http_methods(['GET'])
@api_view(['GET'])
def get_order_by_user(request, pk):
    order_list = OrderData.objects.filter(user_id=pk)
    serializer = OrderDataGetMenuSerializer(order_list, many=True)

    send_logs('get_order_by_user', {}, '', pk, serializer)

    return Response(
        {"data": serializer.data}
    )

@require_http_methods(['POST'])
@api_view(['POST'])
def create_order(request, *args, **kwargs):
    data = request.data
    data['restaurant_name'] = get_restaurant_name(data['restaurant_id'])
    serializer = OrderDataSerializer(data=data)
    if serializer.is_valid():
        serializer.save()

        # add array of object menu list
        create_menu_list(serializer.data['id'], request.data['menu_id_list'])

        # call restaurant service to start cooking
        call_restaurant_service(serializer.data['id'], request.data)

        # call delivery service
        create_delivery_service(serializer.data['id'])

        send_logs('create_order', data, {}, '', serializer)

        return Response(
            {"data": serializer.data},
            status=status.HTTP_201_CREATED
        )
    else:
        return Response(
            {"error": serializer.errors},
            status=status.HTTP_400_BAD_REQUEST
        )
    
# ------------------------------------------------

def create_delivery_service(id):
    payload = {
        'id': id
    }
    response = requests.post('https://delivery-service-law.herokuapp.com/delivery', json=payload)
    response_dict = response.json()

    # print(response_dict)

def get_restaurant_name(id):
    response = requests.get('https://restaurant-service-law.herokuapp.com/restaurant-data/{}/'.format(id))
    response_dict = response.json()

    return response_dict['data']['name']

def create_menu_list(id_order, menu_list):
    for id_menu in menu_list:
        data = {
            'order': id_order,
            'menu_id': id_menu,
        }
        serializer = OrderMenuDataSerializer(data=data)
        if serializer.is_valid():
            serializer.save()

def call_restaurant_service(id, data):
    payload = {
        'order_id': id,
        'restaurant_id': data['restaurant_id'],
        'menu_id_list': data['menu_id_list']
    }
    print(payload)
    response = requests.post('https://restaurant-service-law.herokuapp.com/order-food/create/', json=payload)
    # response = requests.post('http://127.0.0.1:8000/order-food/create/', json=payload)
    response_dict = response.json()

    # print(response_dict)

# ---------------------------------------------------

BASE_URL = "https://order-service-law.herokuapp.com/order"
LOG_URL = "https://logs-01.loggly.com/inputs/c33818a3-eb2d-4b4f-8d89-6dae5e3993c1/tag/http"

VIEWS_URL_MAP = {
    'get_order_by_id': {
        'url': '',
        'method': 'GET'
    },
    'get_order_by_user': {
        'url': 'list/',
        'method': 'GET'
    },
    'create_order': {
        'url': 'create',
        'method': 'POST'
    }
}

def send_logs(view_name, payload, additional_headers, additional_url_path, response):
    if os.environ.get('IS_PRODUCTION_ENV') == 'False':
        return

    method = VIEWS_URL_MAP[view_name]["method"]
    url = VIEWS_URL_MAP[view_name]["url"]

    payload = {
        'service': 'ORDER_SERVICE',
        'method': method,
        'endpoint': f"{BASE_URL}/{url}{additional_url_path}",
        'payload': payload,
        'additional_headers': additional_headers,
        'response_data': response.data,
    }
    print('-----------------------------')
    print('log payload:',payload)

    result = requests.post(LOG_URL, data=json.dumps(payload))
    if result.status_code != 200:
        print("Order service failed sending logs :(")

    print("Order service successfully sending logs!")

