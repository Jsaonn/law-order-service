from rest_framework import serializers
from .models import OrderData, OrderMenuData


class OrderMenuDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderMenuData
        fields = '__all__'

class OrderDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderData
        fields = '__all__'

class OrderDataGetMenuSerializer(serializers.ModelSerializer):
    orderMenuData = OrderMenuDataSerializer(many=True)

    class Meta:
        model = OrderData
        fields = (
            'id',
            'user_id',
            'restaurant_id',
            'restaurant_name',
            'status',
            'orderMenuData',
        )