from django.contrib import admin
from .models import OrderData, OrderMenuData

# Register your models here.
admin.site.register(OrderData)
admin.site.register(OrderMenuData)