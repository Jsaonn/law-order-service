from django.db import models


class OrderData(models.Model):
    user_id = models.IntegerField()
    restaurant_id = models.IntegerField()
    restaurant_name = models.CharField(max_length=255, default="")
    status = models.CharField(
        max_length=255,
        default="Pesanan diterima"
    )

    def __str__(self) -> str:
        return 'order {} - resto {} - status: {}'.format(self.user_id, self.restaurant_id, self.status)


class OrderMenuData(models.Model):
    order = models.ForeignKey(
        OrderData,
        on_delete=models.CASCADE,
        related_name="orderMenuData"
    )
    menu_id = models.IntegerField()

    def __str__(self) -> str:
        return 'order {} - menu {}'.format(str(self.order.id), self.menu_id)