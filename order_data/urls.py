from django.urls import path
from . import views

app_name = 'order_data'
urlpatterns = [
    path('<int:pk>', views.get_order_by_id, name="orderViewWithId"),
    path('list/<int:pk>', views.get_order_by_user, name="orderViewWithUserId"),
    path('create', views.create_order, name="orderCreate")
]